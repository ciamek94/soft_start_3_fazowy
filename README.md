# Układ łagodnego rozruchu

## Opis działania układu

Działanie układu polega na płynnej zmianie kąta wysterowania triaków co zapewnia płynną regulację wartości napięcia skutecznego na zaciskach odbiornika. Układ rozpoczyna pracę w momencie podania zasilania. Kąt wysterowania 3 triaków jest stopniowo zmniejszany co powoduje wzrost napięcia.
Układ mikroprocesorowy oparty jest na mikrokontrolerze STM32F103C8T6. Sterowanie pracą triaków wykonywane jest przez optotriaki serii MOC. Algorytm sterowania polega na wykrywaniu przez mikrokotroler momentów, w których napięcie w każdej z 3 faz przechodzi przez zero. Od tych momentów timer sprzętowy odlicza czas, po którym następuje podanie impulsu na bramkę określonego triaka. W chwili, gdy wartość napięcia wyjściowego osiągnie wartość napięcia zasilania układ załącza dodatkowy triak, do którego może być dołączony stycznik, który z kolei bocznikuje triaki w efekcie układ podłączony jest bezpośrednio do sieci zasilającej nie powodując strat cieplnych wywołanych na skutek przepływu prądu elektrycznego przez strukturę półprzewodnika (triak). Czas rozruchu jest definiowany w programie. Na podstawie wyżej opisanego układu powstał układ łagodnego rozruchu przystosowany do pracy z odbiornikami jednofazowymi.

![](./Soft_start_images/IMG_20210425_154211667_HDR.jpg )

Układ tego typu może być stosowany do rozruchu np. nieobciążonych silników asynchronicznych o charakterystyce obciążenia generatorowej lub wentylatorowej. Stosowanie układów softstarterowych pozwala na zmniejszenie prądu rozruchowego co wpływa z kolei na zmniejszenie zaburzeń napięcia w sieci zasilającej oraz zmniejszenie sił dynamicznych działających na elementy silnika oraz urządzenie napędzane w momencie rozruchu.

![](./Soft_start_images/IMG_20210425_154859475_HDR.jpg ) )


