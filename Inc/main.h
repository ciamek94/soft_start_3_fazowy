/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define LICZBA_KROKOW 300
#define FAZ_POCZ 115 //faza poczatkowa (min napiecie) (czasami dzialalo 120)
#define MAX_FAZA 270 // faza maksymalna (maks napiecie)
#define FAZ_GRAN 100 // faza pomiedzy kt�ra wystepuje praca tr�j lub dwu fazowa
#define CZAS_ROZRUCHU 85 //CZAS_ROZRUCHU*(MAX_FAZA - FAZ_POCZ) ms
#define CZAS_ZAL_STYCZNIKA 150 //(200*10ms)
#define SZER_IMP 110 // szerokosc impulsu wyzwalajacego triaki(1-300) 300 => 10ms (przy pracy tr�jfazowej)
#define SZER_IMP2 10 	// szerokosc impulsu wyzwalajacego triaki(1-300) 300 => 10ms (przy pracy dwufazowej)
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define EXTI_L1_Pin GPIO_PIN_0
#define EXTI_L1_GPIO_Port GPIOA
#define EXTI_L1_EXTI_IRQn EXTI0_IRQn
#define EXTI_L2_Pin GPIO_PIN_1
#define EXTI_L2_GPIO_Port GPIOA
#define EXTI_L2_EXTI_IRQn EXTI1_IRQn
#define EXTI_L3_Pin GPIO_PIN_2
#define EXTI_L3_GPIO_Port GPIOA
#define EXTI_L3_EXTI_IRQn EXTI2_IRQn
#define STYCZNIK_Pin GPIO_PIN_3
#define STYCZNIK_GPIO_Port GPIOB
#define L3_Pin GPIO_PIN_4
#define L3_GPIO_Port GPIOB
#define L2_Pin GPIO_PIN_5
#define L2_GPIO_Port GPIOB
#define L1_Pin GPIO_PIN_6
#define L1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
